# Base image
FROM debian:latest

# Minimum required packages
RUN apt-get update && apt-get install -y \ 
	wget \
	cmake \
	python3 \
	rpm \
&& rm -rf /var/lib/apt/lists/*

# Download and install ppc64 toolchain
RUN wget http://artifactory.esss.lu.se/artifactory/yocto/toolchain/ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-2.6-4.14-77f33945.sh
RUN chmod +x ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-2.6-4.14-77f33945.sh
RUN sh -c '/bin/echo -e "/opt/ifc14xx/2.6-4.14\nY\n" | ./ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-2.6-4.14-77f33945.sh'
RUN rm ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-2.6-4.14-77f33945.sh

# Source toolchain environmental variables after boot (need to be using bash)
#RUN /bin/echo 'source /opt/ifc14xx/2.6-4.14/environment-setup-ppc64e6500-fsl-linux' >> /root/.bashrc

# Add entrypoint 
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh /entrypoint.sh # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]

# Runs bash in absence of any other command
CMD /bin/bash
